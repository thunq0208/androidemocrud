  package com.example.studentlist;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

  public class MainActivity extends AppCompatActivity {
    Button btn_add, btn_viewall, btn_update;
    EditText st_name, st_age;
    ListView student_list;
    ArrayAdapter studentArrayAdapter;
    DataBaseHelper dataBaseHelper;


      @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_add = findViewById(R.id.btn_add);
        btn_viewall = findViewById(R.id.btn_viewall);
        btn_update = findViewById(R.id.btn_update);
        st_name = findViewById(R.id.st_name);
        st_age = findViewById(R.id.st_age);
        student_list = findViewById(R.id.student_list);

        dataBaseHelper = new DataBaseHelper(MainActivity.this);

        ShowStudentsOnListView(dataBaseHelper);

        btn_add.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                StudentModel studentModel;
                try {
                    studentModel = new StudentModel(-1, st_name.getText().toString(), Integer.parseInt(st_age.getText().toString()));
                    Toast.makeText(MainActivity.this, studentModel.toString(), Toast.LENGTH_SHORT).show();

                }
                catch (Exception e){
                    Toast.makeText(MainActivity.this, "Error adding student", Toast.LENGTH_SHORT).show();
                    studentModel = new StudentModel(-1, "error", 0);

                }

                DataBaseHelper dataBaseHelper = new DataBaseHelper(MainActivity.this);
                boolean checkAdd = dataBaseHelper.addOne(studentModel);
                Toast.makeText(MainActivity.this, "Success= " + checkAdd, Toast.LENGTH_SHORT).show();

                ShowStudentsOnListView(dataBaseHelper);
            }
        });

          btn_update.setOnClickListener(new View.OnClickListener(){
              @Override
              public void onClick(View v) {
                  StudentModel studentModel;
                  try {
                      studentModel = new StudentModel(-1, st_name.getText().toString(), Integer.parseInt(st_age.getText().toString()));
                      Toast.makeText(MainActivity.this, studentModel.toString(), Toast.LENGTH_SHORT).show();

                  }
                  catch (Exception e){
                      Toast.makeText(MainActivity.this, "Error update student", Toast.LENGTH_SHORT).show();
                      studentModel = new StudentModel(-1, "error", 0);

                  }

                  DataBaseHelper dataBaseHelper = new DataBaseHelper(MainActivity.this);
                  boolean checkUpdate = dataBaseHelper.updateOne(studentModel);
                  Toast.makeText(MainActivity.this, "Success= " + checkUpdate, Toast.LENGTH_SHORT).show();

                  ShowStudentsOnListView(dataBaseHelper);
              }
          });

        btn_viewall.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dataBaseHelper = new DataBaseHelper(MainActivity.this);

                studentArrayAdapter = new ArrayAdapter<StudentModel>(MainActivity.this, android.R.layout.simple_list_item_2, dataBaseHelper.getAll());
                student_list.setAdapter(studentArrayAdapter);

                //Toast.makeText(MainActivity.this, studentList.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        student_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                StudentModel clickedStudent = (StudentModel) parent.getItemAtPosition(position);
                dataBaseHelper.deleteOne(clickedStudent);
                ShowStudentsOnListView(dataBaseHelper);
                Toast.makeText(MainActivity.this, "Deleted " + clickedStudent, Toast.LENGTH_SHORT).show();

            }
        });

    }

      private void ShowStudentsOnListView(DataBaseHelper dataBaseHelper2) {
          studentArrayAdapter = new ArrayAdapter<StudentModel>(MainActivity.this, android.R.layout.simple_list_item_1, dataBaseHelper2.getAll());
          student_list.setAdapter(studentArrayAdapter);
      }
  }